package com.pajk.moonshine.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RiskTask implements JavaDelegate {
	private static Logger logger = LoggerFactory.getLogger(RiskTask.class);

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		logger.info("风控机审");
	}
}
