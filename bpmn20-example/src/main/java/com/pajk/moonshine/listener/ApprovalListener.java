package com.pajk.moonshine.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApprovalListener implements ExecutionListener {

	private static Logger logger = LoggerFactory.getLogger(ApprovalListener.class);
	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		logger.info("notify()");
		String userApproved = (String) execution.getVariable("userApproved");
		if ("1".equals(userApproved)) {
			logger.info("通过");
		} else if ("2".equals(userApproved)) {
			logger.info("拒绝");
		} else if ("3".equals(userApproved)) {
			logger.info("转移");
		} else {
			logger.error("error");
		}
	}
}
