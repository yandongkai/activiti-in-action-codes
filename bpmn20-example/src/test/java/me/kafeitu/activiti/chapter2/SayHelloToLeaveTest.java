package me.kafeitu.activiti.chapter2;

import org.activiti.engine.*;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.*;

public class SayHelloToLeaveTest {

    @Test
    public void testStartProcess() throws Exception {
        // build
        ProcessEngine processEngine = ProcessEngineConfiguration
                .createStandaloneInMemProcessEngineConfiguration()
                .buildProcessEngine();

        RepositoryService repositoryService = processEngine.getRepositoryService();
        String bpmnFileName = "me/kafeitu/activiti/helloworld/SayHelloToLeave.bpmn";
        // deploy
        repositoryService.createDeployment()
                .addInputStream("SayHelloToLeave.bpmn",
                        this.getClass().getClassLoader().getResourceAsStream(bpmnFileName))
                .deploy();
        // query
        ProcessDefinition processDefinition = repositoryService
                .createProcessDefinitionQuery().singleResult();
        assertEquals("SayHelloToLeave", processDefinition.getKey());

        RuntimeService runtimeService = processEngine.getRuntimeService();

        Map<String, Object> variables = new HashMap<>();
        // 申请人的名称
        variables.put("applyUser", "employee1");
        // 请假天数
        variables.put("days", 3);
        // start
        ProcessInstance processInstance = runtimeService
                // activiti在启动的时候会把这两个变量存入数据库中，之后可以通过接口读取到节点
                // TODO activit将variables存到哪里，之后查询是怎么获取到的？
                .startProcessInstanceByKey("SayHelloToLeave", variables);
        assertNotNull(processInstance);
        System.out.println("pid=" + processInstance.getId() + ", pdid="
                + processInstance.getProcessDefinitionId());

        TaskService taskService = processEngine.getTaskService();
        Task taskOfDeptLeader = taskService.createTaskQuery()
                // TODO Candidate Users，Candidate Groups，Assignee分别的区别？
                .taskCandidateGroup("deptLeader")
                .singleResult();

        System.out.println(taskOfDeptLeader);
        assertNotNull(taskOfDeptLeader);
        assertEquals("领导审批", taskOfDeptLeader.getName());
        // 声明任务责任：给定用户成为该任务的受让人。
        // 不会检查身份组件是否知道用户
        taskService.claim(taskOfDeptLeader.getId(), "leaderUser");
        variables = new HashMap<>();
        variables.put("approved", true);
        taskService.complete(taskOfDeptLeader.getId(), variables);

        taskOfDeptLeader = taskService.createTaskQuery()
                .taskCandidateGroup("deptLeader")
                .singleResult();
        // 再次查询为空
        assertNull(taskOfDeptLeader);

        HistoryService historyService = processEngine.getHistoryService();
        long count = historyService.createHistoricProcessInstanceQuery()
                .finished()
                .count();
        assertEquals(1, count);
    }
}