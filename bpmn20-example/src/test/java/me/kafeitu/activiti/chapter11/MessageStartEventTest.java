/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.kafeitu.activiti.chapter11;

import jodd.util.ThreadUtil;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.*;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.EventSubscriptionQueryImpl;
import org.activiti.engine.impl.persistence.entity.EventSubscriptionEntity;
import org.activiti.engine.impl.test.PluggableActivitiTestCase;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.Deployment;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.java2d.pipe.SpanIterator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 消息启动事件
 * <p> 通过一个特定的消息标志触发事件
 *
 * @author henryyan
 */
public class MessageStartEventTest extends PluggableActivitiTestCase {
    private static Logger logger = LoggerFactory.getLogger(MessageStartEventTest.class);

    /**
     * 通过消息名称启动
     */
    @Deployment(resources = "chapter11/messageEvent/messageStartEvent.bpmn")
    public void testStartMessageEvent() throws Exception {
        ProcessInstance processInstance = runtimeService
                .startProcessInstanceByMessage("启动XXX流程", "001");
        assertNotNull(processInstance);
        System.out.println("businessKey: " + processInstance.getBusinessKey());
        System.out.println("DefinitionId: " + processInstance.getProcessDefinitionId());
        System.out.println("id: " + processInstance.getId());
    }

    @Test
    @Deployment(resources = {"chapter11/messageEvent/moonshine.bpmn"})
    public void testListener() {
        logger.info("+++++++++++ 开始测试 +++++++++++++");
        saveUserAndGroup();
        // TODO 在流程启动时可以动态设置候选人
        ProcessInstance processInstance = runtimeService
                .startProcessInstanceByMessage("启动内容审核系统审核流程", "0001");
        logger.info("businessKey: {}", processInstance.getBusinessKey());
        logger.info("DefinitionId: {}", processInstance.getProcessDefinitionId());
        logger.info("id: {}",  processInstance.getId());

        Task task = taskService.createTaskQuery()
                .taskCandidateGroup("auditUser")
                .singleResult();
        logger.info("task: {}", task);
        String taskId = task.getId();
        logger.info("taskId: {}", taskId);
        // 当前用户claim该任务后，该任务无法被其他用户获取
        logger.debug("start claim");
        taskService.claim(taskId, "auditUser1"); // TODO 通过数据库来控制，支持分布式
        logger.debug("end claim");

        getHistory(processInstance);

        logger.debug("select other user start");
        // 测试其他的用户获取不到该 task
        Task task1 = taskService.createTaskQuery()
                .taskCandidateGroup("auditUser")
                .singleResult();
        logger.info("task1: {}", task1);

        sleep(3);

        // TODO 定时 如果任务没有被处理则释放

        sleep(3);

        taskService.claim(taskId, "auditUser1");
        taskService.unclaim(taskId);

        getHistory(processInstance);

        Map<String, Object> variables = new HashMap<>();
        variables.put("userApproved", "2");
//        taskService.complete(task.getId(), variables);
        deleteUserAndGroup();
        logger.info("+++++++++++ 结束测试 +++++++++++++");
    }

    public void sleep(int time) {
        try {
            TimeUnit.SECONDS.sleep(time);
        } catch (InterruptedException e) {
        }
    }

    public void getHistory(ProcessInstance processInstance) {
        // 查询历史的流程实例
        List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery()
                .processInstanceId(processInstance.getId())
                .list();
        // 查询所有的历史活动记录
        List<HistoricActivityInstance> list1 = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(processInstance.getId())
                .list();
        // 查询流程有关的变量
        List<HistoricVariableInstance> list2 = historyService.createHistoricVariableInstanceQuery()
                .processInstanceId(processInstance.getId())
                .list();

        List<HistoricTaskInstance> list3 = historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(processInstance.getId())
                .list();

        logger.info("list: {}", list);
        logger.info("list1: {}", list1);
        logger.info("list2: {}", list2);
        logger.info("list3: {}", list2);
    }

    public void deleteUserAndGroup() {
        identityService.deleteMembership("auditUser1", "auditUser");
        identityService.deleteMembership("providerManager1", "providerManager");
        identityService.deleteGroup("auditUser");
        identityService.deleteGroup("providerManager");
        identityService.deleteGroup("admin");
        identityService.deleteUser("auditUser1");
        identityService.deleteUser("providerManager1");
    }

    public void saveUserAndGroup() {
        // auditUser
        Group group = identityService.newGroup("auditUser");
        group.setName("审核人员");
        group.setType("auditUser");
        identityService.saveGroup(group);
        // providerManager
        Group group1 = identityService.newGroup("providerManager");
        group1.setName("供应商管理员");
        group1.setType("providerManager");
        identityService.saveGroup(group1);
        // admin
        Group group2 = identityService.newGroup("admin");
        group2.setName("运营");
        group2.setType("admin");
        identityService.saveGroup(group2);

        // 添加审核人员
        User auditUser1 = identityService.newUser("auditUser1");
        auditUser1.setFirstName("auditUser1");
        auditUser1.setLastName("auditUser1");
        auditUser1.setEmail("auditUser1@qq.com");
        identityService.saveUser(auditUser1);

        User providerManager1 = identityService.newUser("providerManager1");
        providerManager1.setFirstName("providerManager1");
        providerManager1.setLastName("providerManager1");
        providerManager1.setEmail("providerManager1@qq.com");
        identityService.saveUser(providerManager1);

        identityService.createMembership("auditUser1", "auditUser");
        identityService.createMembership("providerManager1", "providerManager");
    }

    /**
     * 通过流程ID启动
     */
    @Deployment(resources = "chapter11/messageEvent/messageStartEvent.bpmn")
    public void testStartMessageEventByKey() throws Exception {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("messageStartEvent");
        assertNotNull(processInstance);
    }

    /**
     * 启动前检查对应的消息是否已注册到引擎
     */
    @Deployment(resources = "chapter11/messageEvent/messageStartEvent.bpmn")
    public void testMessageSubcription() throws Exception {
        EventSubscriptionQueryImpl eventSubscriptionQuery = new EventSubscriptionQueryImpl(processEngineConfiguration.getCommandExecutor());
        EventSubscriptionEntity subscriptionEntity = eventSubscriptionQuery.eventName("启动XXX流程").singleResult();
        System.out.println(subscriptionEntity.getConfiguration());
        System.out.println(subscriptionEntity.getCreated());
        assertNotNull(subscriptionEntity);
    }

}
