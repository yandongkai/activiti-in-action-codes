package me.kafeitu.activiti.chapter7;

import me.kafeitu.activiti.base.AbstractTest;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.test.Deployment;
import org.junit.Test;

public class MoonshineTest extends AbstractTest {

    @Test
    @Deployment(resources = {"diagrams/chapter7/moonshine.bpmn"})
    public void testListener() {

        ProcessInstance processInstance = runtimeService
                .startProcessInstanceByMessage("启动内容审核系统审核流程", "0001");

    }

}
